//
//  AppConstants.swift
//  ProjectViewerMVP
//
//  Created by Aishat Olowoshile on 10/20/19.
//  Copyright © 2019 Aishat Olowoshile. All rights reserved.
//

import Foundation


class AppContants {
    
    //Enums
    enum AddActionMode {
        case cart
        case list
    }
    
    //Enums
    enum ShipMode {
        case ship
        case b2
    }
}
