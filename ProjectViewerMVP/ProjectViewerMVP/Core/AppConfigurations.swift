//
//  AppConfigurations.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/14/19.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation


class AppConfigurations {
    
    //Environment Information
    static func baseURLScheme() -> String {
        return "https"
    }
    
    static func baseURLhost() -> String {
        return "target-deals.herokuapp.com"
    }
    
    static func baseURLPath() -> String {
        return "/api/deals"
    }
    
    static func httpMethodType() -> String {
        return "GET"
    }
    
    
    
}

enum HttpMethod: String {
    //TODO: ADD MORE CASES AS NEEDED
    case get = "GET"
}
