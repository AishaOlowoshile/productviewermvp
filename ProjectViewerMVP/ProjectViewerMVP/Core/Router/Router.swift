//
//  Router.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/15/19.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation

enum Router {
    // cases represents request endpoints
    case getProductList
    case getProductDetail
    
    
    var scheme: String {
        switch self {
        case .getProductList, .getProductDetail:
            return AppConfigurations.baseURLScheme()
        }
    }
    
    var host: String {
        switch self {
        case .getProductList, .getProductDetail:
            return AppConfigurations.baseURLhost()
        }
    }
    
    var path: String {
        switch self {
        case .getProductList:
            return AppConfigurations.baseURLPath()
        case .getProductDetail:
            return AppConfigurations.baseURLPath()
        }
    }
    
    var method: String {
        switch self {
        case .getProductList, .getProductDetail:
            return AppConfigurations.httpMethodType()
        }
    }
}
