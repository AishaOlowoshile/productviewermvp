//
//  HttpGateway.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/16/19.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation
import Alamofire

class HttpGateway {
    // MARK: - HTTP Requests
    func REQUEST(_ type: HTTPMethod, _ urlString: String, completion: @escaping( _ response: Data?, _ error: Error?) -> Void ) {
        
        Alamofire.request(urlString, method: type, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().response { (response) in
             completion(response.data, response.error)
        }
    }
}
