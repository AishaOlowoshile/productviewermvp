//
//  Product.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/14/19.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation
import ObjectMapper


class ProductResponse: Mappable {
    var data: [Product]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class Product: Mappable {
    var title: String?
    var description: String?
    var image: String?
    var salePrice: String?
    var price: String?
    var guide: String?
    var aisle: String?
    var index: Int?
    var id: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        description <- map["description"]
        image <- map["image"]
        salePrice <- map["salePrice"]
        price <- map["price"]
        guide <- map["guid"]
        aisle <- map["aisle"]
        index <- map[""]
        id <- map["_id"]
    }
}
