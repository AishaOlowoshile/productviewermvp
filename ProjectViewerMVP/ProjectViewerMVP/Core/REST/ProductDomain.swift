//
//  ProductDomain.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/14/19.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ProductDomain {
    //Singleton
    static let shared = ProductDomain()
    fileprivate let gateway = HttpGateway()
    //var url: String?
}

//Network Calls
extension ProductDomain {
    // MARK: Get Product
    func getProducts(completion: @escaping(_ result: ProductResponse?, _ error: String?) -> Void ) {
        var httpMethod: HTTPMethod {
            return (AppConfigurations.httpMethodType() == HttpMethod.get.rawValue) ? HTTPMethod.get : HTTPMethod.get
        }
        
        func buildURL() -> String? {
            var components = URLComponents()
            components.scheme = Router.getProductList.scheme
            components.host = Router.getProductList.host
            components.path = Router.getProductList.path
            
            guard let url = components.url else { return nil }
            return url.absoluteString
        }
        
        guard let urlString = buildURL() else { return }
        gateway.REQUEST(httpMethod, urlString) { [weak self] (data, error) in
            self?.handle(objectClass: ProductResponse.self, response: data, error: error, completion: completion)
        }
    }
    
    //handle response
    fileprivate func handle<O: Mappable>(objectClass: O.Type, response: Data?, error: Error?, completion handler: (_ result: O?, _ error: String?) -> Void) {
        
        //error case
        if let error = error {
            handler(nil, error.localizedDescription)
            return
        }
        
        //success: No body
        if response == nil && error == nil {
            handler(nil, nil)
            return
        }
        
        //deserialize JSON Array
        func deserialize<T: Mappable>(_ objectClass: T.Type, _ response: Data) -> T? {
            guard response.count > 0 else {
                return nil
            }
            
            do {
                let dictionary = try JSONSerialization.jsonObject(with: response, options: .allowFragments) as! NSDictionary
                return T(JSON: dictionary as! [String: Any])
            } catch {
                print(error)
                print("ProductDomain: Unable to find a JSON string.")
            }
            
            return nil
        }
        
        //success: with response body
        if let jsonArray = deserialize(O.self, response!) {
            handler(jsonArray, nil)
            return
        }
        
        handler(nil, "Unable to read server response.")
    }
}
