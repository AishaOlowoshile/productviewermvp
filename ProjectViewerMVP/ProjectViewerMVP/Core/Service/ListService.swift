//
//  ListService.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/14/19.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation
import Alamofire

class ListService {
    //Singleton
    static var shared = ListService()
}

extension ListService {
    func getProducts(completion: @escaping (_ result: ProductResponse?, _ error: String?) -> Void) {
        ProductDomain.shared.getProducts { (products, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            completion(products, nil)
        }
    }
}
