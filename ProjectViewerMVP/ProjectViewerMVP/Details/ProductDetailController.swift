//
//  ProductDetailController.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/17/19.
//  Copyright © 2019 Target. All rights reserved.
//

import UIKit
import CellDataCoupler

class ProductDetailController: BaseViewController {
    
    var presenter: ProductDetailPresenter!
    
    @IBOutlet var tableView: UITableView!
    var tableSource: BaseTableSource?
    
    
    //Initializer
    static func create(product: Product) -> ProductDetailController {
        let vc = UIStoryboard(name: "ProductDetail", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailController") as! ProductDetailController
        vc.presenter = ProductDetailPresenter(view: vc, product: product)
        vc.title = "Product Description"
        return vc
    }
    
    //Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableSource()
        presenter.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func setupTableSource() {
        tableSource = BaseTableSource(with: tableView)
    }
    
    func showShareOptions(_ image: UIImage) {
        let imageToShare = [ image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension ProductDetailController: DetailView {
    func showAlert(message: String) {
        self.showAlertController(message: message)
    }
    
    func retrievedDetailInformation(_ product: Product) {
        var couplers = [BaseCellCoupler]()
        let productInfoData = ProductDetailCellData(product: product, handleAddToList: { [weak self] in
            //handleAddToList
            self?.presenter.handleAddAction(productID: product.id, addActionMode: .list)
        }, handleAddToCart: { [weak self] in
            //handleAddToCart
            self?.presenter.handleAddAction(productID: product.id, addActionMode: .cart)
        }) { [weak self] (image) in
            //handleAddShareButtonTapped
            self?.showShareOptions(image)
        }
        
        let productDetailCell = CellCoupler(ProductDetailCell.self, productInfoData)
        couplers.append(productDetailCell)
        tableSource?.set(couplers: couplers)
    }
}
