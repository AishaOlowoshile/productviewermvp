//
//  ProductDetailCell.swift
//  ProjectViewerMVP
//
//  Created by Aishat Olowoshile on 10/20/19.
//  Copyright © 2019 Aishat Olowoshile. All rights reserved.
//

import UIKit
import SDWebImage
import CellDataCoupler


struct ProductDetailCellData {
    var product: Product?
    var handleAddToList: (() -> Void)
    var handleAddToCart: (() -> Void)
    var handleAddShareButtonTapped: ((_ image: UIImage) -> Void)
    
}

class ProductDetailCell: ReusableCell<ProductDetailCellData> {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var addToListButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    
    var productImage: UIImage?
    
    override func setup() {
        setUpUI()
        
        guard let product = info?.product else { return }
        
        descriptionLabel.text = product.description
        priceLabel.text = (product.salePrice != nil) ? product.salePrice : product.price
        
        productImageView?.sd_setImage(with: URL(string:  product.image ?? ""), placeholderImage: UIImage(named: "Icon-App-20x20")) { [weak self] (image, _, _, _) in
            if let _ = image {
                self?.productImageView.image = image
                self?.productImage = image
            }
        }
    }
    
    func setUpUI() {
        addToCartButton.layer.cornerRadius = 5
        addToListButton.layer.cornerRadius = 5
        
        productImageView.contentMode = .scaleAspectFill
    }
    
    @IBAction func addToListTapped(_ sender: Any) {
        info?.handleAddToList()
    }
    
    @IBAction func addToCartTapped(_ sender: Any) {
        info?.handleAddToCart()
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        guard let image = self.productImage else { return }
        info?.handleAddShareButtonTapped(image)
    }
    
}
