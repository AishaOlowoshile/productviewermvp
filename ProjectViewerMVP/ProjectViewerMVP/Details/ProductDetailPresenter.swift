//
//  ProductDetailPresenter.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/17/19.
//  Copyright © 2019 Target. All rights reserved.
//

import UIKit

protocol DetailView: class {
    func retrievedDetailInformation(_ product: Product)
    func showAlert(message: String)
}

class ProductDetailPresenter {
    
    weak var view: DetailView?
    var currentProduct: Product?
    var productIdsForList = [String]()
    var productIdsForCart = [String]()
    
    //Initialization
    init(view: DetailView, product: Product) {
        self.view = view
        self.currentProduct = product
    }
    
    func start() {
        refresh()
    }
    
    func refresh() {
        guard let product = currentProduct else { return }
        self.view?.retrievedDetailInformation(product)
    }
    
    func handleAddAction(productID: String?, addActionMode: AppContants.AddActionMode) {
        guard let id = productID else { return }
        switch addActionMode {
        case .cart:
            if let index = productIdsForCart.firstIndex(where: { $0 == id }) {
                self.productIdsForCart.remove(at: index)
                self.view?.showAlert(message: "Product has been removed from cart.")
            } else {
                self.productIdsForCart.append(id)
                self.view?.showAlert(message: "Product has been added to cart.")
            }
        case .list:
            if let index = productIdsForList.firstIndex(where: { $0 == id }) {
                self.productIdsForList.remove(at: index)
                self.view?.showAlert(message: "Product has been removed from list.")
            } else {
                self.productIdsForList.append(id)
                self.view?.showAlert(message: "Product has been added to list.")
            }
        }
    }
    
}
