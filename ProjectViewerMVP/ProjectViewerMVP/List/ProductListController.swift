//
//  ProductListController.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/17/19.
//  Copyright © 2019 Target. All rights reserved.
//

import UIKit
import CellDataCoupler

class ProductListController: BaseViewController {
    var presenter: ProductListPresenter!
    
    @IBOutlet var tableView: UITableView!
    
    var tableSource: BaseTableSource?
    
    //Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProductListPresenter(view: self)
        setupTableSource()
        presenter.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func setupTableSource() {
        tableSource = BaseTableSource(with: tableView)
    }
    
    func setupUI() {
        self.tableView.tableHeaderView = nil;
        self.tableView.tableFooterView = nil;
    }
}

extension ProductListController: ListView {
    func retrievedItems(_ products: [Product]) {
        tableSource?.set(couplers: cellCouplersFor(products))
    }
    
    func showAlert(message: String) {
        self.showAlertController(message: message)
    }
    
    func showError(_ message: String) {
        self.showErrorDialog(error: message)
    }
    
    //Navigation
    func navigateToProductDetail(_ product: Product) {
        let vc = ProductDetailController.create(product: product)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showSpinner() {
        CustomSpinner.shared.show(vc: self)
    }
    
    func stopSpinner() {
        CustomSpinner.shared.stop(vc: self)
    }
}

extension ProductListController {
    func cellCouplersFor(_ products: [Product]) -> [BaseCellCoupler] {
        var cellCouplers = [BaseCellCoupler]()
        
        for product in products {
            let cellData = ProductListCellData(product: product) { [weak self] (mode) in
                self?.presenter.handleShipOption(mode)
            }
            let productCell = CellCoupler(ProductListCell.self, cellData) { [weak self] (_) in
                self?.presenter?.handleProductSelection(product)
            }
            cellCouplers.append(productCell)
        }
        return cellCouplers
    }
}





