//
//  ProductListPresenter.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/17/19.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation
import SpinnerView

protocol ListView: class {
    func showError(_ message: String)
    func retrievedItems(_ products: [Product])
    func navigateToProductDetail(_ product: Product)
    func showSpinner()
    func stopSpinner()
    func showAlert(message: String)
}


class ProductListPresenter {
    weak var view: ListView?
    var productList = [Product]()
    
    //Initialization
    init(view: ListView) {
        self.view = view
    }
    
    func start() {
        getProductList()
    }
    
    func refreshUI() {
        self.view?.retrievedItems(productList)
    }
    
    func handleProductSelection(_ product: Product) {
        self.view?.navigateToProductDetail(product)
    }
    
    func handleShipOption(_ mode: AppContants.ShipMode ) {
        switch mode {
        case .ship:
            self.view?.showAlert(message: "Ship Option Selected")
        case .b2:
            self.view?.showAlert(message: "B2 Option Selected")
        }
    }
}

//WebCalls
extension ProductListPresenter {
    func getProductList() {
        
        self.view?.showSpinner()
        
        ListService.shared.getProducts { [weak self] (response, error) in
           
            self?.view?.stopSpinner()
            
            if let error = error {
                self?.view?.showError(error)
                return
            }
            
            guard let productList = response?.data else { return }
            self?.productList = productList
            self?.refreshUI()
            
        }
    }
}


