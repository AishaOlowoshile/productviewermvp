//
//  ProductListCell.swift
//  ProductViewer
//
//  Created by Aishat Olowoshile on 10/18/19.
//  Copyright © 2019 Target. All rights reserved.
//

import UIKit
import CellDataCoupler
import SDWebImage

typealias ProductListCellData = (product: Product?, handleShipOptionTap: ((_ mode: AppContants.ShipMode) -> Void))

class ProductListCell: ReusableCell<ProductListCellData> {
    
    
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var borderedUIView: UIView!
    @IBOutlet weak var productAisleLabel: UILabel!
    @IBOutlet weak var labelContainerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    override func setup() {
        setupUI()
        guard let product = info?.product else { return }
        productTitleLabel.text = product.title
        productPriceLabel.text = (product.salePrice != nil) ? product.salePrice : product.price
        productAisleLabel.text = "Aisle: \(product.aisle ?? "NIS")"
        let contentMode = productImageView.contentMode
        productImageView.contentMode = .scaleAspectFit
        let url = URL(string: product.image ?? "")
        productImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "Icon-App-20x20")) { [weak self] (image, _, _, _) in
            if let _ = image {
                self?.productImageView.contentMode = contentMode
            }
        }
    }
    
    func setupUI() {
        productImageView.contentMode = .scaleAspectFill
        productImageView.layer.cornerRadius = 5.0;
        productImageView.clipsToBounds = true;
        labelContainerView.layer.cornerRadius = 5.0;
        labelContainerView.clipsToBounds = true;
    }
    
    @IBAction func shipButtonTapped(_ sender: Any) {
        info?.handleShipOptionTap(.ship)
    }
    
    @IBAction func b2ButtonTapped(_ sender: Any) {
        info?.handleShipOptionTap(.b2)
    }
    
}
