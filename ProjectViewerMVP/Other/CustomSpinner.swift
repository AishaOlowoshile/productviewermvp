//
//  CustomSpinner.swift
//  ProjectViewerMVP
//
//  Created by Aishat Olowoshile on 10/20/19.
//  Copyright © 2019 Aishat Olowoshile. All rights reserved.
//

import UIKit
import SpinnerView

class CustomSpinner {
    static var shared = CustomSpinner()
    let spinner = SpinnerView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    
    func show(vc: UIViewController) {
        let centerX = vc.view.bounds.width / 2
        let centerY = vc.view.bounds.height / 2
        spinner.center = CGPoint(x: centerX, y: centerY)
        spinner.successColor = UIColor.red
        spinner.strokeColor = UIColor.red
        spinner.hidesWhenStopped = true
        vc.view.addSubview(spinner)
        UIApplication.shared.beginIgnoringInteractionEvents()
        spinner.start()
    }
    
    func stop(vc: UIViewController!) {
        //vc.view.isUserInteractionEnabled = true
        DispatchQueue.main.async {
            UIApplication.shared.endIgnoringInteractionEvents()
            self.spinner.stop(success: true)
        }
    }
}
