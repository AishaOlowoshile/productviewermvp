//
//  BaseTableView.swift
//  ProjectViewerMVP
//
//  Created by Aishat Olowoshile on 10/20/19.
//  Copyright © 2019 Aishat Olowoshile. All rights reserved.
// Header & Footer Source: Cell Data Coupler Example

import UIKit
import CellDataCoupler

class BaseTableSource: CouplerTableSource {
    
    //UI
    var estimatedHeaderHeights: [Int: CGFloat] = [:]
    var estimatedFooterHeights: [Int: CGFloat] = [:]
    
    override init(with tableview: UITableView) {
        super.init(with: tableview)
        tableview.tableFooterView = UIView()
    }
    
    //Header Setup
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        estimatedHeaderHeights[section] = view.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    
    //Footer Setup
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        estimatedFooterHeights[section] = view.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         return CGFloat.leastNormalMagnitude
    }
    
    //Cell Steup
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 50, 0)
        cell.layer.transform = transform
        cell.alpha = 0
        UIView.animate(withDuration: 0.5) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1.0
        }
    }
}
