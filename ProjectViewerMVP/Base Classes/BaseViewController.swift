//
//  BaseViewController.swift
//  ProjectViewerMVP
//
//  Created by Aishat Olowoshile on 10/19/19.
//  Copyright © 2019 Aishat Olowoshile. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var alertController: UIAlertController?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func showAlertController(message: String? = nil) {
        // Show Loading
        alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        if let alertController = alertController {
            present(alertController, animated: false, completion: nil)
        }
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] (action) in
            self?.hideAlert()
        }
        alertController?.addAction(okAction)
    }
    
    func hideAlert() {
        alertController?.dismiss(animated: true, completion: nil)
    }
    
    func showErrorDialog(error: String?) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: false, completion: nil)
    }
}
