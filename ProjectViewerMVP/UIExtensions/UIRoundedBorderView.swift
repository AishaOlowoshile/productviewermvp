//
//  UIBorderedView.swift
//  ProjectViewerMVP
//
//  Created by Aishat Olowoshile on 10/19/19.
//  Copyright © 2019 Aishat Olowoshile. All rights reserved.
//

import UIKit

@IBDesignable
class UIRoundedBorderView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateBorder()
    }
    
    @IBInspectable var cornerRadius: CGFloat = 5.0 {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.5 {
        didSet {
            updateBorder()
        }
    }
    
    func updateBorder() {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
    }
}


@IBDesignable
class CircleBackgroundView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
         updateBorder()
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.5 {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateBorder()
        }
    }
    
    func updateBorder() {
        layer.cornerRadius = bounds.size.width / 2
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
    }
}
